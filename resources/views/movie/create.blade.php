<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<h1>Create new Movie</h1><br>
	<form method="POST" action="/movie">
		{{ csrf_field() }}
		
		<input type="text" name="title" placeholder="Title Movie"><br><br>
		<input type="text" name="body" placeholder="Content Movie"><br><br>
		<button type="submit">Create</button>
	</form>
</body>
</html>