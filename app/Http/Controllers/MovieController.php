<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
class MovieController extends Controller
{
    public function index() {
    	$movies = Movie::all();
    	return view('movie.index', compact('movies'));
    }


    public function create() {
    	return view('movie.create');
    }

    public function store()
    {
    	$movie = new Movie();

    	$movie->title = request('title');
    	$movie->body = request('body');
    	$movie->save();

    	return redirect('/movie');
    }
}
