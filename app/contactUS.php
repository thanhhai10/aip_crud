<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contactUS extends Model
{
    public $table = 'contact_us';
    protected $fillable = ['name','email','message'];
}
