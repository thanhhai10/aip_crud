<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('contact-us', 'ContactUSController@contactUS');
Route::post('contact-us', ['as'=>'contactus.store','uses'=>'ContactUSController@contactUSPost']);


Route::get('/task', function () {
	$tasks = [
		'Go to store',
		'Go to market',
		'Go to'
	];
return view('task')->withTasks($tasks)->withFoo('foo');
});


Route::get('/movie', 'MovieController@index');
Route::post('/movie', 'MovieController@store');
Route::get('/movie/create', 'MovieController@create');